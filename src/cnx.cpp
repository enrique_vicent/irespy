#include "cnx.h"
#include <ESP8266WiFi.h>

WifiConnection::WifiConnection(){
    WiFi.mode(WIFI_AP_STA);
}

void WifiConnection::APStart(String SSID, String pwd){
    if(ApPasswd.length() ){
        WiFi.softAP(ApSID, ApPasswd);
    } else {
        WiFi.softAP(ApSID);
    }
    if (SSID != NULL && SSID.length() > 0){
        Serial.println("trying to connect ..");
        if (pwd != NULL && pwd.length() > 0){
            Serial.println("trying to connect .. with passwd");
            WiFi.begin(SSID, pwd);
        } else {
            WiFi.begin(SSID);
        }
        Serial.println("waiting to connected");
        WiFi.onStationModeConnected([](const WiFiEventStationModeConnected& idc ){
            Serial.println("connected, with no ip...");
        });
        WiFi.onStationModeGotIP(onConnect);
    }  

}

IPAddress WifiConnection::getAPIP(){
    return (WiFi.softAPIP());
}
