#include <Arduino.h>
#include <string>
#include <sstream>
#include "lcd.h"
#include "button.h"
#include "irSensor.h"
#include "cnx.h"
#include "web.h"
#include "storage.h"
#include "irLed.h"

#define BAUD 76800
#define BUTTONPIN 2
#define IRSENSORPIN 13
#define IRLEDPIN 15
#define LCDSCLPIN 14 
#define LCDSDAPIN 12

Lcd lcd(LCDSDAPIN, LCDSCLPIN);
Button btn(BUTTONPIN);
IRSensor irSensor(IRSENSORPIN);
IRLed irLed(IRLEDPIN);
WifiConnection wifiCnx;
WebServer www;
Storage storage;

auto onIRRead = [](decode_results signal){
  std::string out_string;
  std::stringstream ss;
  ss << signal.value;
  out_string = ss.str();
  lcd.displayCapture(IRSensor::protocolLiteral(signal), out_string);
};

auto onclick = []() { 
  std::string out_string;
  std::stringstream ss;
  lcd.displayWelcome("sending"); 
  auto lastRead = irSensor.result;
  ss << lastRead.value;
  ss << " sent";
  out_string = ss.str();
  irLed.send(lastRead.decode_type,lastRead.value,lastRead.bits,lastRead.repeat); //TODO: esto es totalmente dummy
  lcd.displayCapture(IRSensor::protocolLiteral(lastRead), out_string);
};

auto onLongClick = []() { 
  //TODO: disconnect when connected
  wifiCnx.APStart(storage.data.ssid, storage.data.passwd);
  string ip (wifiCnx.getAPIP().toString().c_str());
  lcd.displayWelcome(ip); 
};

auto onWifiConnected = [](const WiFiEventStationModeGotIP& ipdata ){
  string ipString (ipdata.ip.toString().c_str());
  lcd.displayWelcome(ipString);
};

auto httpConnectRequest = [](String ssid, String pwd){
  strncpy(storage.data.ssid,ssid.c_str(),25);
  strncpy(storage.data.passwd,pwd.c_str(),25);
  storage.writeData();
  btn.onLongClick();
  String result = "connecting";
  return result;
};


void setup() {
  Serial.begin(BAUD);
  Serial.println("starting App");
  lcd.init();  
  btn.init();
  btn.onClick = onclick;
  btn.onLongClick= onLongClick;
  lcd.displayWelcome("botón");
  irSensor.init();
  irSensor.onReceive = onIRRead;
  irLed.init();
  lcd.displayWelcome("ir");
  www.init();
  www.cmdConnectHandler = httpConnectRequest;
  wifiCnx.onConnect = onWifiConnected;
  lcd.displayWelcome("web");
  storage.begin();
  lcd.displayWelcome("eeprom");
  lcd.displayWelcome("ready");
}

void loop() {
  unsigned long time = millis();
  btn.timer(time);
  irSensor.timer();
  www.timer();
  yield();
}