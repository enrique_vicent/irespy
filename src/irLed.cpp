#include "irLed.h"


IRLed::IRLed(char pin): irsend (pin), pin(pin){
}

void IRLed::init(){
    digitalWrite(this->pin, LOW);
    
}

void IRLed::send(const decode_type_t type, const uint64_t data,
                  const uint16_t nbits, const uint16_t repeat){
    #ifdef debug
    Serial.print("[IRLed] - IR SENT> protcl ");
    irsend.begin();  
    Serial.print(type);
    Serial.print(" +b ");
    Serial.print(nbits);
    Serial.print(" +r ");
    Serial.print(repeat);
    Serial.print(" +data ");
    serialPrintUint64(data, HEX);
    Serial.println();
    #endif

    irsend.send(type, data, nbits, repeat);
    digitalWrite(this->pin, LOW);
}