#include "lcd.h"
#include <Arduino.h>
#include <Wire.h>


void Lcd::init() {
    u8g2.begin();
};

void Lcd::powerSave(bool save){
    u8g2.setPowerSave(save);
}

void Lcd::displayWelcome(string version){
    u8g2.clearBuffer();					// clear the internal memory
    u8g2.setFont(u8g2_font_ncenB08_tr);	// choose a suitable font
    u8g2.drawStr(0,10,"IR ESP SPY");	// write something to the internal memory
    u8g2.drawStr(0,20,"watch over IR comms");  // write something to the internal memory
    u8g2.drawStr(0,30,"by Ecnil");  // write something to the internal memory
    u8g2.drawHLine(4,52,u8g2.getDisplayWidth() -8 );
    u8g2.drawStr(50,40,__DATE__);  // write something to the internal memory
    u8g2.drawStr(0, 62, version.c_str());
    u8g2.sendBuffer();					// transfer internal memory to the display
    return;
};

void Lcd::displayCapture(string protocol, string code){
    u8g2.clearBuffer();					// clear the internal memory
    u8g2.setFont(u8g2_font_ncenB08_tr);	// choose a suitable font
    u8g2.drawStr(0,10," -IR SIGNAL CAPTURED-");	// write something to the internal memory
    u8g2.drawStr(0,20,"protocol");  // write something to the internal memory
    u8g2.drawStr(10,30,protocol.c_str());  // write something to the internal memory
    u8g2.drawStr(0,40,"value");  // write something to the internal memory
    u8g2.drawStr(10,50,code.c_str());  // write something to the internal memory
    u8g2.drawHLine(4,52,u8g2.getDisplayWidth() -8 );
    u8g2.drawStr(0, 62, "press button to send it");
    u8g2.sendBuffer();					// transfer internal memory to the display
    return;
};
