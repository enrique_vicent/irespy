#include "irSensor.h"
#include <IRremoteESP8266.h>


IRSensor::IRSensor(int pin) : irrecv(pin) {
    gpioPin = pin;
    irrecv = IRrecv(pin);
};

void IRSensor::init(){
    irrecv.enableIRIn();
};

void IRSensor::timer(){
    if(irrecv.decode(&result)) {
        #ifdef debug
        Serial.print("[IRSensor] - IR RECV!");
        serialPrintUint64(result.value, HEX);
        Serial.println();
        #endif

        if(onReceive != NULL) onReceive(result);
        irrecv.resume();
    }
};

void IRSensor::power(bool on){
    if(on){
        irrecv.disableIRIn();
    } else {
        irrecv.enableIRIn();
    }
};


char * IRSensor::protocolLiteral( const decode_results &results){
     
  if (results.decode_type == UNKNOWN) {
    return "Unknown";
  } else if (results.decode_type == NEC) {
    return "NEC";
  } else if (results.decode_type == SONY) {
    return "SONY";
  } else if (results.decode_type == RC5) {
    return "RC5";
  } else if (results.decode_type == RC5X) {
    return "RC5X";
  } else if (results.decode_type == RC6) {
    return "RC6";
  } else if (results.decode_type == RCMM) {
    return "RCMM";
  } else if (results.decode_type == PANASONIC) {
    return "PANASONIC - Address ?";;// + std:: (result.address) ;
  } else if (results.decode_type == LG) {
    return "LG";
  } else if (results.decode_type == JVC) {
    return "JVC";
  } else if (results.decode_type == AIWA_RC_T501) {
    return "AIWA RC T501";
  } else if (results.decode_type == WHYNTER) {
    return "Whynter";
  } else if (results.decode_type == NIKAI) {
    return "Nikai";
  }
}