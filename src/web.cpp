#include "web.h"
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#define _WEB_H_ROOT "<html>\n<head>\n<!-- convert using https://tomeko.net/online_tools/cpp_text_escape.php?lang=en -->\n<template id=\"ir-read-template\">\n  <style>\n    :host { display: flex; flex-direction: row; justify-content: space-evenly;}\n    .read-label {flex-basis: 10%; }\n    .read-value {flex-basis: 20%; }\n    button { background-color: aqua; }\n    \n  </style>\n    <span class=\"read-label\">Protocol</span>\n    <span class=\"read-value\" id=\"protocol\"></span>\n    <span class=\"read-label\">Signature</span>\n    <span class=\"read-value\" id=\"value\"></span>\n    <button id=\"send\">Send</button>\n</template>\n  \n<script>\n  const SERVICE_READINGS = \"./reads\";\n  const SERVICE_EMIT = \"./sendit\"\n  const SERVICE_CONNECT = \"./connect\"; //post {ssid, pwd} //get {ssid}\n  \n  window.customElements.define('ir-read', class extends HTMLElement{\n    constructor(){\n      super();\n      let shadowRoot = this.attachShadow({mode: 'open'});\n      const t = document.querySelector('#ir-read-template');\n      const instance = t.content.cloneNode(true);\n      shadowRoot.appendChild(instance);\n    }\n    get irProtocol (){ return this.getAttribute('protocol'); }\n    get irValue () { return this.getAttribute('value'); }\n    get irNum () { return this.getAttribute('index'); }\n    requestSend(){\n      var url = `${SERVICE_EMIT}?protocol=${this.irProtocol}&value=${this.irValue}`;\n      fetch(url);\n    }\n    connectedCallback(){\n      this.shadowRoot.getElementById(\"protocol\").innerHTML = this.irProtocol;\n      this.shadowRoot.getElementById(\"value\").innerHTML = this.irValue;\n      this.shadowRoot.getElementById(\"send\").addEventListener('click', e => {\n        this.requestSend()\n      })\n    }\n  });\n  \n  window.customElements.define('ir-reading-list', class extends HTMLElement{\n    constructor(){\n      super();\n      let shadowRoot = this.attachShadow({mode: 'open'});\n      shadowRoot.innerHTML = `\n        <style>\n          :host { display: grid; }\n        </style>\n        <slot></slot>\n        <button id=\"refresh\">refresh</button>\n      `;\n    }\n    \n    refreshView(json){\n      this.innerHTML='';\n      json.reads.forEach( (x,y) => {\n          var irRead = document.createElement(\"ir-read\");\n          irRead.setAttribute('index', y+1);\n          irRead.setAttribute('protocol', x.protocol);\n          irRead.setAttribute('value', x.value);\n          this.append(irRead);\n      })\n    }\n    \n    fetchReadings(){\n      var that = this;\n      var fetchPromise = fetch(SERVICE_READINGS);\n      fetchPromise.then( (response ) => {\n        response.json().then( (json) => {\n          that.refreshView(json);\n        });  \n      })\n    }\n    \n    connectedCallback(){\n      this.fetchReadings();\n      var button = this.shadowRoot.getElementById(\"refresh\");\n      button.addEventListener(\"click\", e => {\n        this.fetchReadings();\n      });\n    }\n  });\n  \n  window.customElements.define('wifi-connect', class extends HTMLElement{\n    constructor(){\n      super();\n      let shadowRoot = this.attachShadow({mode: 'open'});\n      this.SSID = '';\n      this.render();\n    }\n    \n    template(){\n      return `\n        <style>\n          :host { display: grid; }\n        </style>\n        <p>connect to:</p>\n        <div class=\"wifi-connect-form\">\n          <div>SSID <input type=\"text\" id=\"issid\" value=\"${this.SSID}\"/></div>\n          <div>Password <input type=\"password\" id=\"ipwd\"/></div>\n          <div><button id=\"btncnx\">Connect and Save</button></div>\n        </div>\n      `;\n    }\n    \n    render(){\n      this.shadowRoot.innerHTML = this.template();\n      var button = this.shadowRoot.getElementById(\"btncnx\");\n      button.addEventListener(\"click\", e => {\n        this.connectCmd();\n      });\n    }\n    \n    connectCmd(){\n      let valSID=this.shadowRoot.getElementById(\"issid\").value\n      let valPwd=this.shadowRoot.getElementById(\"ipwd\").value\n      if(valSID){\n        fetch(SERVICE_CONNECT, {\n          method: 'POST',\n          body: JSON.stringify({\n            ssid:valSID, \n            pwd:valPwd\n          })\n        }).then( response => { \n          return response.json(); \n        }).then( json => {\n          console.info(json);\n        })\n      }\n    }\n    \n    fetchWifi(){\n      let that = this;\n      let fecthPromise = fetch(SERVICE_CONNECT)\n      fecthPromise.then( response => {return response.json()} ).then (json => {\n        let theSid = json.ssid\n        if(theSid){\n          that.SSID = theSid;\n          that.render();\n        }\n      })\n    }\n        \n    connectedCallback(){\n      this.fetchWifi();\n    }\n  });\n\n</script>\n<title>InfraRed Reads</title>\n</head>\n<body>\n  <h1>Readings</h1>\n  <p>most recent reading first, click \"send\" to reply the reading</p>\n  <ir-reading-list></ir-reading-list>\n  <h1>Wifi</h1>\n  <p>connect to wifi:</p>\n  <p>you can connect to the wifi called IRSPY (http://192.168.4.1) and also you can connect this device to the wifi or your choice:</p>\n  <wifi-connect></wifi-connect>\n</body>\n</html>"

WebServer::WebServer() {
}

void WebServer::init(){
    const String urlRoot = "/" ;
    auto capturedServer = &myServer;
    WebServer *that = this;
    auto rootHaandler = [capturedServer](){
        capturedServer->send(
            200, 
            "text/html", 
            _WEB_H_ROOT);
        return;
    };
    myServer.on("/", rootHaandler);
    myServer.on("/reads", [capturedServer](){
        capturedServer->send(
            200, 
            "application/json", 
            "{}");
        return;
    });
    myServer.on("/connect", HTTP_GET, [capturedServer](){
        capturedServer->send(
            200, 
            "application/json", 
            "{}");
        return;
    });
        myServer.on("/connect", HTTP_POST, [capturedServer,that](){
        Serial.println("requested connect");
        StaticJsonDocument<100>doc;
        auto requestBody  = capturedServer->arg("plain");
        deserializeJson(doc,requestBody);
        String ssid = doc["ssid"];
        Serial.println(ssid);
        String passwd = doc["pwd"];
        Serial.println(passwd);
        if (that->cmdConnectHandler){
            Serial.println("call connect");
            String response = that->cmdConnectHandler(ssid, passwd);
            capturedServer->send(
            200, 
            "application/json", 
            "{\"result\":\"" + response + "\"}");
        } else {
            capturedServer->send(
            500, 
            "application/json", 
            "{}");
        }
        return;
    });
    myServer.begin();
}

void WebServer::timer(){
    this->myServer.handleClient();
}
