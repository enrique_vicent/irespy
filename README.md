PCB
==========

- circuit & board https://easyeda.com/eccnil/irespy 
- thing: https://www.thingiverse.com/thing:3804404 
- case design sources: https://a360.co/2YwAKIV 

Generate `.photon` file
---------

Generate the photonfile (and isolate the board with Anycubic Photom) is very easy, few Steps needed

1. at EasyEda generate a *Black over White* png file of the pcb, at maximun size
1. edit the file to trim the frame, this will help to get the proper size when slicing
1. *compile* the model (pcb_sla.scad) with openSCAD
1. Slice with Photon slicer. 
    - Put the piece upside-down to print only the first layer. 
    - Time of first layer 600s
    - As you trimmed the png, now you can scale the picture to the target size
    - Print only the first layer

open the file with openSCAD and:
verify:

- Text shold be readable (un-mirrored)
- tracks are deep and the rest raised

you can export the file simply runing:

```bash
OpenScad -o pcb_sla.stl pcb_sla.scad 
````
