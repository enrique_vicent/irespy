#ifndef _IRLED_H_
#define _IRLED_H_

#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <IRutils.h>
//#define debug 1

class IRLed
{
private:
    IRsend irsend;
    char pin;
public:
    IRLed(char pin);
    void init();
    void send(const decode_type_t type, const uint64_t data,
                  const uint16_t nbits, const uint16_t repeat);

};

#endif