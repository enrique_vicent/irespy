#ifndef _LCD_H_
#define _LCD_H_

#include <string.h>
#include <U8g2lib.h>

using namespace std;

class Lcd{
    U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2;
public:
    Lcd() : u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE) {};
    Lcd(u8 sda, u8 scl): u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE, scl, sda) {}
    void init();
    void powerSave(bool);
    void displayWelcome(string);
    void displayCapture(string protocol, string code);
    void displaySending(string protocol, string code);
    void displayWifi();
};

#endif