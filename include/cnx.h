#ifndef _CNX_H_
#define _CNX_H_
#include <functional>
#include <ESP8266WiFi.h>


typedef std::function<void(const WiFiEventStationModeGotIP&)> CnxEvent; //took from http://tedfelix.com/software/c++-callbacks.html

class WifiConnection {
    String ApSID = "IRSPY";
    String ApPasswd = "";
public:
    WifiConnection();
    void APStart( String SSID, String pwd );
    void APStop( );
    IPAddress getAPIP();
    CnxEvent onConnect;
};

#endif