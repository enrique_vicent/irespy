#ifndef _WEB_H_
#define _WEB_H_
#include <functional>
#include <ESP8266WebServer.h>


typedef std::function<String (String ssid, String passwd)> WebActionConnect; //took from http://tedfelix.com/software/c++-callbacks.html

class WebServer {
    ESP8266WebServer myServer;
public:
    WebServer();
    void init();
    void timer();
    WebActionConnect cmdConnectHandler;
};

#endif