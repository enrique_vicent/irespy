#ifndef _IRSENSOR_H_
#define _IRSENSOR_H_


#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRutils.h>
//see: https://github.com/markszabo/IRremoteESP8266
//#define debug 1

typedef std::function<void(const decode_results)> IRrecvEvent; //took from http://tedfelix.com/software/c++-callbacks.html


class IRSensor{
    IRrecv irrecv;
    int gpioPin;
public:
    decode_results result;
    IRSensor(int pin);
    void init ();
    void timer ();
    void power(bool);
    IRrecvEvent onReceive = NULL;
    static char* protocolLiteral(const decode_results &protocol);
};
#endif